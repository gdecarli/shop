package com.gabrieledecarli.shop.shop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.gabrieledecarli.shop.shop.domain.IFiscalDocument;
import com.gabrieledecarli.shop.shop.domain.IRetriveTaxesStrategy;
import com.gabrieledecarli.shop.shop.domain.Order;
import com.gabrieledecarli.shop.shop.domain.Receipt;
import com.gabrieledecarli.shop.shop.domain.TaxRoundingStrategy;
import com.gabrieledecarli.shop.shop.service.IPrintService;

/**
 * Demo shop App
 * 
 */
public class App {
	public static ApplicationContext	applicationContext	= new AnnotationConfigApplicationContext(AppConfiguration.class);	;

	public static void main(String[] args) {
		
		IPrintService<IFiscalDocument> service = (IPrintService<IFiscalDocument>) applicationContext.getBean("printService");
		IRetriveTaxesStrategy taxStrategy = (IRetriveTaxesStrategy) applicationContext.getBean("taxStrategy");
		IPrintService<Receipt> recivePrintService = (IPrintService<Receipt>) applicationContext.getBean("RecivePrintService");
	
		Order order1=(Order)applicationContext.getBean("order1");
		Order order2=(Order)applicationContext.getBean("order2");
		Order order3=(Order)applicationContext.getBean("order3");
		service.print(order1);
		service.print(order2);
		service.print(order3);
		Receipt receipt1 = Receipt.create("Output 1",order1, taxStrategy,TaxRoundingStrategy.create());
		Receipt receipt2 = Receipt.create("Output 2", order2, taxStrategy, TaxRoundingStrategy.create());
		Receipt receipt3 = Receipt.create("Output 3", order3, taxStrategy, TaxRoundingStrategy.create());
		recivePrintService.print(receipt1);
		recivePrintService.print(receipt2);
		recivePrintService.print(receipt3);
	}
}
