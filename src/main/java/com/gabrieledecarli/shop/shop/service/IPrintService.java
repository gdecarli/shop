package com.gabrieledecarli.shop.shop.service;

import com.gabrieledecarli.shop.shop.domain.IFiscalDocument;

/**
 * Common interface for definition of the common behavior for different kind of print
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public interface IPrintService <T extends IFiscalDocument> {
	void print(T doc );
}
