package com.gabrieledecarli.shop.shop.service;

import java.io.PrintStream;

import com.gabrieledecarli.shop.shop.domain.Receipt;

public class ReceiptPrintService extends DefaultPrintService<Receipt> {
	public static ReceiptPrintService create(PrintStream out){
		return new ReceiptPrintService(out);
	}
	protected ReceiptPrintService(PrintStream out){
		super(out);
	}
	
	@Override
	protected String getSeparator(){
		return ":";
	}
	@Override
	protected void printEndingTag(Receipt doc) {
		out.println("Sales Taxes: "+doc.getTaxes().getAmount().toString());
		out.println("Total: "+doc.getTotal().getAmount().toString());
		out.println("------------ End ----------");
	}
}
