package com.gabrieledecarli.shop.shop.service;

import java.io.PrintStream;

import com.gabrieledecarli.shop.shop.domain.EntryRow;
import com.gabrieledecarli.shop.shop.domain.IFiscalDocument;

public class DefaultPrintService<T extends IFiscalDocument> implements IPrintService<T> {
  protected final PrintStream out;
	public static DefaultPrintService<? extends IFiscalDocument> create(PrintStream out) {
    return new DefaultPrintService<IFiscalDocument>(out);
  }
	protected DefaultPrintService(PrintStream out){
		this.out=out;
	}

  
	protected void printRow(EntryRow entry) {
		out.println(entry.getQuantity()+" " +entry.getProduct().getName()+getSeparator()+entry.getTotalAmount().getAmount().toString() );
	}
	
	protected String getSeparator(){
		return " at ";
	}

	protected void printHeading(T doc) {
		System.out.println("-----------"+doc.getReference()+"-----------");
		
	}

	protected void printEndingTag(T doc) {
		System.out.println("------------ End ----------");
	}
	public void print(T doc) {
		this.printHeading(doc);
		for(EntryRow entry : doc.getEntries()){
			this.printRow(entry);
		}
		printEndingTag(doc);
	}
	
}
