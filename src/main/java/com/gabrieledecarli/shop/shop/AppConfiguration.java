package com.gabrieledecarli.shop.shop;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import com.gabrieledecarli.shop.shop.domain.BasicTaxRetriveStrategy;
import com.gabrieledecarli.shop.shop.domain.CurrencyAmount;
import com.gabrieledecarli.shop.shop.domain.IFiscalDocument;
import com.gabrieledecarli.shop.shop.domain.IRetriveTaxesStrategy;
import com.gabrieledecarli.shop.shop.domain.Order;
import com.gabrieledecarli.shop.shop.domain.Product;
import com.gabrieledecarli.shop.shop.domain.ProductOrigin;
import com.gabrieledecarli.shop.shop.domain.ProductType;
import com.gabrieledecarli.shop.shop.domain.Receipt;
import com.gabrieledecarli.shop.shop.service.DefaultPrintService;
import com.gabrieledecarli.shop.shop.service.IPrintService;
import com.gabrieledecarli.shop.shop.service.ReceiptPrintService;

@Configuration
@ComponentScan(basePackages = "com.gabrieledecarli.shop.shop")
// @ImportResource({"classpath:appbase-context.xml"})
public class AppConfiguration {
	private DataSource	datasource	= null;

	@Bean(name = "dataSource")
	public DataSource dataSource() {
		if (datasource == null) {
			this.datasource = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).setName("provided_shop_database").build();
		}
		return datasource;
	}

	@Bean(name = "taxStrategy")
	public IRetriveTaxesStrategy taxStrategy() {
		return new BasicTaxRetriveStrategy();
	}

	@Bean(name = "printService")
	public IPrintService<? extends IFiscalDocument> printService() {
		return DefaultPrintService.create(System.out);
	}

	@Bean(name = "RecivePrintService")
	public IPrintService<Receipt> recivePrintService() {
		return ReceiptPrintService.create(System.out);
	}
	@Bean(name = "order1")
	public Order order1() {
		Product book1 = Product.create("Book", ProductType.BOOK, ProductOrigin.LOCAL, CurrencyAmount.money("12.49"));
		Product cd1 = Product.create("music CD", ProductType.MUSIC, ProductOrigin.LOCAL, CurrencyAmount.money("14.99"));
		Product chocolate = Product.create("chocolate bar", ProductType.FOOD, ProductOrigin.LOCAL, CurrencyAmount.money("0.85"));
		Order order1 = Order.create("Input 1");
		order1.add(book1, 1);
		order1.add(cd1, 1);
		order1.add(chocolate, 1);
		return order1;
	}
	@Bean(name="order2")
	public Order order2(){
		Product impChoco = Product.create("Imported Chocolate", ProductType.FOOD, ProductOrigin.IMPORTED, CurrencyAmount.money("10.00"));
		Product impPerfume = Product.create("Imported Perfume", ProductType.GENERIC, ProductOrigin.IMPORTED, CurrencyAmount.money("47.50"));
		Order order2 = Order.create("Input 2");
		order2.add(impChoco, 1);
		order2.add(impPerfume, 1);
		return order2;
	}
	@Bean(name="order3")
	public Order order3(){
		Product impPerfume1 = Product.create("bottle of perfume", ProductType.GENERIC, ProductOrigin.LOCAL, CurrencyAmount.money("18.99"));
		Product impPerfume2 = Product.create("Imported Perfume", ProductType.GENERIC, ProductOrigin.IMPORTED, CurrencyAmount.money("27.99"));
		Product pills = Product.create("packet of headache pills", ProductType.MEDICAL, ProductOrigin.LOCAL, CurrencyAmount.money("9.75"));
		Product impChoco = Product.create("Imported Chocolate", ProductType.FOOD, ProductOrigin.IMPORTED, CurrencyAmount.money("11.25"));
		Order order3 = Order.create("Input 3");
		order3.add(impPerfume2, 1);
		order3.add(impPerfume1, 1);
		order3.add(pills, 1);
		order3.add(impChoco, 1);
		return order3;
	}
}
