package com.gabrieledecarli.shop.shop.domain;
/**
 * abstraction due to common functionality for "listed product elements" elements
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public interface EntryRow {
	 Product getProduct();
	 CurrencyAmount getTotalAmount();
	 Integer getQuantity();
}
