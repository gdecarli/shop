package com.gabrieledecarli.shop.shop.domain;
/**
 * Representing a Product a product have a name, an origin, and a untaxed price
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public class Product {
	public static Product create(String name, ProductType type, ProductOrigin origin, CurrencyAmount price) {
		return new Product(name, type, origin, price);
	}

	private final String				name;
	private final ProductType		type;
	private final ProductOrigin	origin;
	private final CurrencyAmount					prize;

	private Product(String name, ProductType type, ProductOrigin origin, CurrencyAmount shalfPrice) {
		this.name = name;
		this.type = type;
		this.origin = origin;
		this.prize = shalfPrice;
	}

	public String getName() {
		return name;
	}

	public ProductType getType() {
		return type;
	}

	public ProductOrigin getOrigin() {
		return origin;
	}

	public CurrencyAmount getPrice() {
		return prize;
	}
	public Boolean isImported(){
		return this.origin.equals(ProductOrigin.IMPORTED);
	}
}
