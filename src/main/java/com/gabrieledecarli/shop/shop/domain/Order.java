package com.gabrieledecarli.shop.shop.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public class Order implements IFiscalDocument {
	private String orderReference;
	protected final List<OrderEntry>	entries	= new ArrayList<OrderEntry>();

	/*
	 * Factory Method
	 */
	public static Order create(String orderReference) {
		return new Order(orderReference);
	}

	public void add(Product product, int quantity) {
		OrderEntry entry = OrderEntry.create(product, product.getPrice(), quantity);
		entries.add(entry);

	}

	protected Order(String orderReference) {
		this.orderReference=orderReference;
	}


	public CurrencyAmount getTotal() {
		CurrencyAmount total = CurrencyAmount.money("0.00");
		for (EntryRow entry : entries) {
			total = total.add(entry.getTotalAmount());
		}
		return total;
	}

	public List<OrderEntry> getEntries() {
		return entries;
	}

	public void setReference(String orderReference) {
		this.orderReference = orderReference;
	}

	public String getReference() {
		return orderReference;
	}
}
