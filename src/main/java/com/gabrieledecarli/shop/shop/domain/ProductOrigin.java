package com.gabrieledecarli.shop.shop.domain;

/**
 * Define the origin of a Product, is base responsibility is to trace the provenance of an object, it's been modeled like an
 * enumeration in order to be simple, may be a candidate to became more complex (eg for instance in the case we want to provide
 * this software service through a SaaS environment at different shop in different country)
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public enum ProductOrigin {
	LOCAL, IMPORTED
}
