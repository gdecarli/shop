package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Untaxed extends AbstractTax {
	public static Untaxed create(){
		return new Untaxed();
	}
	private Untaxed(){
		super();
		BigDecimal zero=new BigDecimal(0);
		zero.setScale(2,RoundingMode.HALF_UP);
		super.setRate(zero);
	}
	public CurrencyAmount Apply(ReceiptEntry entry) {
		return entry.getTotalAmount();
	}



}
