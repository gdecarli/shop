package com.gabrieledecarli.shop.shop.domain;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public class Receipt implements IFiscalDocument {
	protected final List<ReceiptEntry>	taxedEntries	= new ArrayList<ReceiptEntry>();
	protected String										reference;

	public static Receipt create(String reference) {
		return new Receipt(reference);
	}

	/*
	 * Can be improved passing at more advanced implementation of FactoryPattern
	 */
	public static Receipt create(String reference,Order order,IRetriveTaxesStrategy taxStrategy,ITaxRoundingStrategy taxRoundingStrategy ) {
		Receipt created = Receipt.create(reference);
		for (OrderEntry orderEntry : order.getEntries()) {
			created.add(ReceiptEntry.create(orderEntry, taxStrategy,taxRoundingStrategy));
		}
		return created;
	}

	private Receipt(String reference) {
		this.reference=reference;
	}

	public void add(ReceiptEntry entry) {
		this.taxedEntries.add(entry);
	}

	public List<ReceiptEntry> getEntries() {
		return taxedEntries;
	}

	public String getReference() {
		return this.reference;
	}

	public CurrencyAmount getTotal() {
		CurrencyAmount total = CurrencyAmount.money("0.00");
		for (EntryRow entry : taxedEntries) {
			total = total.add(entry.getTotalAmount());
		}
		return total;
	}
	public CurrencyAmount getTaxes(){
		CurrencyAmount taxes=CurrencyAmount.money("0");
		for(ReceiptEntry entry: taxedEntries){
			taxes=taxes.add(entry.calculateTaxedAmount());
		}
		return taxes;
	}
}
