package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;

public interface ITaxRoundingStrategy {
	public BigDecimal round(BigDecimal amount);
}
