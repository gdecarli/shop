package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.util.List;

public abstract class AbstractTax implements ITax{
	protected List<ProductType> applyOn;
	protected BigDecimal rate;

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public CurrencyAmount Apply(ReceiptEntry entry) {
		CurrencyAmount amount= CurrencyAmount.money(entry.getProduct().getPrice().getAmount().multiply(this.getRate()));
		return amount;
	}
	
}
