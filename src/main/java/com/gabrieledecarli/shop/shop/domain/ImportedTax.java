package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ImportedTax extends AbstractTax {
	
	public static ImportedTax create(){
		return new ImportedTax();
	}
	
	private ImportedTax (){
		super();
		BigDecimal rate=new BigDecimal(5);
		rate.setScale(2,RoundingMode.HALF_UP);
		super.setRate(rate);
	}

}
