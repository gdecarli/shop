package com.gabrieledecarli.shop.shop.domain;

/**
 * Common interface for the implementation of a strategy for retrieve the tax for an object, the taxation environment can be related
 * to too many aspect to be bounded to an object or object abstraction and we miss too many information to take a choice, an strategy
 * offer an good basic point to decouple our code, it can evolve in many different way for instance a mere Repository or can hide a rules engine or an
 * indirection to a service.
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public interface IRetriveTaxesStrategy {
	ITax getApplicableTaxes(Product toBeTaxed);
}
