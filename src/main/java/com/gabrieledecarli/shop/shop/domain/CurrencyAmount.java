package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Represents the amount of money, is a ValueObject, wrap a BigDecimal, allow to decouple the amount a money and related information
 * from the object, can be update to take care of different currency if needed, the class name and the constructor name are left
 * generic in that purpose.
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */

public class CurrencyAmount {
	private final BigDecimal	amount;

	/*
	 * Factory Method
	 */
	public static CurrencyAmount money(String amount) {
		return new CurrencyAmount(new BigDecimal(amount));
	}

	public static CurrencyAmount money(BigDecimal amount) {
		return new CurrencyAmount(amount);
	}

	private CurrencyAmount(BigDecimal amount) {
		this.amount = amount.setScale(2, RoundingMode.UP);
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public CurrencyAmount multiply(int times) {
		return multiply(new BigDecimal(times));
	}

	public CurrencyAmount multiply(String factor) {
		return multiply(new BigDecimal(factor));
	}

	public CurrencyAmount multiply(BigDecimal factor) {
		return new CurrencyAmount(amount.multiply(factor));
	}

	/*
	 * Return a new CurrencyAmount resulting of the addition
	 */
	public CurrencyAmount add(CurrencyAmount other) {
		return new CurrencyAmount(amount.add(other.amount));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyAmount other = (CurrencyAmount) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		return true;
	}

}
