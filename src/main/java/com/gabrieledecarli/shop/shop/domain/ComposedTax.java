package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

/**
 * Tax composed by multiple tax, application of the composite pattern, allow to interact with multiple tax polices as if was one, it
 * an simplified approach due to the fact that Taxes Strategy act by sum each other, in a more complex case, it can be changed to a
 * Decorator pattern or if needed a Specification pattern
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public class ComposedTax implements ITax {
	private Set<ITax>	taxes	= new HashSet<ITax>();

	public static ComposedTax create() {
		return new ComposedTax();
	}

	private ComposedTax() {
	}

	public CurrencyAmount Apply(ReceiptEntry entry) {

		return null;
	}

	public BigDecimal getRate() {
		BigDecimal rate = new BigDecimal(0);
		rate.setScale(2, RoundingMode.HALF_UP);
		for (ITax tax : taxes) {
			rate = rate.add(tax.getRate());
		}
		return rate;
	}

	public void add(ITax tax) {
		this.taxes.add(tax);
	}

}
