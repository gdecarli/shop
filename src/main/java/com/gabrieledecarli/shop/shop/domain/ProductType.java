package com.gabrieledecarli.shop.shop.domain;

/**
 * Describe the Type of a Product, its base responsibility is to mark a product with a type, the product type are modeled as
 * enumeration with the intent of take it simple, but are good candidate to be changed in class through a class hierarchy in case of
 * the requirement became more complex in the future (as example add type of product at runtime, different properties or behavior
 * for the different type)
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public enum ProductType {
	BOOK,
  FOOD,
  MEDICAL,
  MUSIC,
  GENERIC
}
