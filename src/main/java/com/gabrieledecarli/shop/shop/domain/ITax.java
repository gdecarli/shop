package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
/**
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public interface ITax {
	public CurrencyAmount Apply(ReceiptEntry entry);
	public BigDecimal getRate();
}
