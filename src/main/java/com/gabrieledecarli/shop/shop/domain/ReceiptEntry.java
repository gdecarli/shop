package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Representing the Taxed element on the invoice, the functionality expose the total cost and is delegated to calculate it 
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */

public class ReceiptEntry implements EntryRow {
	private final OrderEntry			orderEntry;
	private final ITax						taxPolicy;
	private final ITaxRoundingStrategy taxRoundingStrategy;

	private ReceiptEntry(OrderEntry taxedEntry, ITax taxPolicy, ITaxRoundingStrategy taxRoundingStrategy) {
		this.orderEntry = taxedEntry;
		this.taxPolicy = taxPolicy;
		this.taxRoundingStrategy=taxRoundingStrategy;
	}
	private ReceiptEntry(OrderEntry taxedEntry, IRetriveTaxesStrategy strategy,ITaxRoundingStrategy taxRoundingStrategy){
		this.orderEntry = taxedEntry;
		this.taxPolicy = strategy.getApplicableTaxes(this.orderEntry.getProduct());
		this.taxRoundingStrategy=taxRoundingStrategy;
	}

	public static ReceiptEntry create(OrderEntry taxedEntry,  IRetriveTaxesStrategy taxStrategy, ITaxRoundingStrategy taxRoundingStrategy) {
		return new ReceiptEntry(taxedEntry, taxStrategy,taxRoundingStrategy);
	}

	public Product getProduct() {
		return orderEntry.getProduct();
	}


	public ITax getTaxPolicy() {
		return taxPolicy;
	}

	public CurrencyAmount calculateTaxedAmount() {
		BigDecimal rate = this.getTaxPolicy().getRate();
		BigDecimal price = this.orderEntry.getTotalAmount().getAmount();
		BigDecimal result = rate.multiply(price.multiply(new BigDecimal(this.getQuantity()))).divide(new BigDecimal(100),2,RoundingMode.UP);
		result=	taxRoundingStrategy.round(result);
		return CurrencyAmount.money(result);
	}

	public CurrencyAmount getTotalAmount() {
		return calculateTaxedAmount().add(this.orderEntry.getTotalAmount());
	}
	
	public Integer getQuantity(){
		return orderEntry.getQuantity();
	}

}
