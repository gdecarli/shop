package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TaxRoundingStrategy implements ITaxRoundingStrategy {
	public static TaxRoundingStrategy create(){
		return new TaxRoundingStrategy();
	}
	private TaxRoundingStrategy(){
		
	}
	public BigDecimal round(BigDecimal amount) {
		BigDecimal result=amount.divide(new BigDecimal("0.5"),1,RoundingMode.UP).multiply(new BigDecimal("0.5"));
		return result;
	}

}
