package com.gabrieledecarli.shop.shop.domain;


/**
 * Represents the single row on the order
 * 
 * @author gabrieledecarli
 * @mail gabriele.decarli.84@gmail.com
 */
public class OrderEntry implements EntryRow {

	private final CurrencyAmount	price;
	private final Product					product;
	private final int							quantity;

	public static OrderEntry create(Product product, CurrencyAmount price, int quantity) {
		return new OrderEntry(product, price, quantity);
	}

	/*
	 * Factory Method
	 */
	private OrderEntry(Product product, CurrencyAmount shalfPrice, int quantity) {
		this.product = product;
		this.price = shalfPrice;
		this.quantity = quantity;
	}

	public Product getProduct() {
		return this.product;
	}

	public CurrencyAmount getTotalAmount() {
		return price.multiply(quantity);
	}

	public CurrencyAmount getPrice() {
		return price;
	}

	public Integer getQuantity() {
		return quantity;
	}

}
