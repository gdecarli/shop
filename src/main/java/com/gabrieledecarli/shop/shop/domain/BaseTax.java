package com.gabrieledecarli.shop.shop.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BaseTax extends AbstractTax{
	public static BaseTax create(){
		return new BaseTax();
	}
	private BaseTax (){
		super();
		BigDecimal rate=new BigDecimal(10);
		rate.setScale(2,RoundingMode.HALF_UP);
		super.setRate(rate);
	}
}
