package com.gabrieledecarli.shop.shop.domain;

import java.util.List;

public interface IFiscalDocument {
	List<? extends EntryRow> getEntries();

	String getReference();

	CurrencyAmount getTotal();
}
