package com.gabrieledecarli.shop.shop.domain;

public class BasicTaxRetriveStrategy implements IRetriveTaxesStrategy {

	public ITax getApplicableTaxes(Product toBeTaxed) {
		ComposedTax tax=ComposedTax.create();
		if(toBeTaxed.getOrigin().equals(ProductOrigin.IMPORTED))
			tax.add(ImportedTax.create());
		if(!toBeTaxed.getType().equals(ProductType.FOOD)&&!toBeTaxed.getType().equals(ProductType.MEDICAL)&&!toBeTaxed.getType().equals(ProductType.BOOK))
			tax.add(BaseTax.create());
		else {
			tax.add(Untaxed.create());
		}
		return tax;
	}
}
