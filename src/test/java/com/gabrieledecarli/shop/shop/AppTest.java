package com.gabrieledecarli.shop.shop;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.gabrieledecarli.shop.shop.domain.CurrencyAmount;
import com.gabrieledecarli.shop.shop.domain.IRetriveTaxesStrategy;
import com.gabrieledecarli.shop.shop.domain.Order;
import com.gabrieledecarli.shop.shop.domain.ProductOrigin;
import com.gabrieledecarli.shop.shop.domain.ProductType;
import com.gabrieledecarli.shop.shop.domain.Receipt;
import com.gabrieledecarli.shop.shop.domain.ReceiptEntry;
import com.gabrieledecarli.shop.shop.domain.TaxRoundingStrategy;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	public static ApplicationContext	applicationContext	= new AnnotationConfigApplicationContext(AppConfiguration.class);	;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Use Case test
     */
    
    public void testApp()
    {	
    	Order order3=(Order)applicationContext.getBean("order3");
  		IRetriveTaxesStrategy taxStrategy = (IRetriveTaxesStrategy) applicationContext.getBean("taxStrategy");
  		Receipt receipt3 = Receipt.create("Output 1",order3, taxStrategy,TaxRoundingStrategy.create());
  		ReceiptEntry re= receipt3.getEntries().get(0);
  		assertEquals(re.getTotalAmount(), CurrencyAmount.money("32.19"));
  		assertEquals(re.calculateTaxedAmount(), CurrencyAmount.money("4.2"));
  		assertEquals(re.getQuantity(), new Integer(1));
  		assertEquals(re.getProduct().getOrigin(), ProductOrigin.IMPORTED);
  		assertEquals(re.getProduct().getType(), ProductType.GENERIC);
  		assertEquals(receipt3.getTotal(), CurrencyAmount.money("74.68"));
  		assertEquals(receipt3.getTaxes(), CurrencyAmount.money("6.70"));
    }
    
}
